﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Quickset.Model.EDID
{
    public class EDIDInfo
    {        
        #region Properties
        public Int32    SlNo { get; set; }
        public String   Country { get; set; }
        public String   MainDevice { get; set; }
        public String   SubDevice { get; set; }        
        public String   Brand { get; set; }
        public String   Model { get; set; }
        public String   DataSource { get; set; }
        public String   FingerprintToPublishInDAC3 { get; set; }
        public String   CustomFPType0x00 { get; set; }
        public String   CustomFPType0x01 { get; set; }
        public String   CustomFPType0x02 { get; set; }
        public String   CustomFPType0x03 { get; set; }
        public String   CustomFP { get; set; }       
        public String   Only128FP { get; set; }
        public String   ID { get; set; }
        public String   AutoDetectSource { get; set; }
        public String   PublishedInDB { get; set; }
        public String   PublishedInDAC3 { get; set; }
        public String   Rules { get; set; }
        public String   DiscreteProfile { get; set; }
        public String   DiscreteID { get; set; }
        public String   Control { get; set; }
        public String   OSDBlock { get; set; }
        public String   VendorID { get; set; }
        public String   ManufacturerCode { get; set; }
        public String   EDIDBlock { get; set; }
        public String   ProductCode { get; set; }
        public String   ManufacturerHexCode { get; set; }
        public String   MonitorName { get; set; }
        public String   SerialNumber { get; set; }
        public Int32    Week { get; set; }
        public Int32    Year { get; set; }
        public Int32    HorizontalScreenSize { get; set; }
        public Int32    VerticalScreenSize { get; set; }
        public String   VideoInputDefinition { get; set; }
        public String   PresentInXBox { get; set; }
        public Int64    ConsoleCount { get; set; }
        public String   PhysicalAddress { get; set; }
        public List<Byte> EdId { get; set; }
        public List<Byte> OSD { get; set; }                                                                                
        #endregion

        #region Constrcutor
        public EDIDInfo()
        {
        }
        #endregion
    }
}