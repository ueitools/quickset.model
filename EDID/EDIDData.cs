﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Quickset.Model.EDID
{
    public class EDIDData
    {
        private bool EDIDValid = false;
        private byte ReasonForInValidity = 0;
        public byte GetReasonForInValidity()
        {
            return ReasonForInValidity;
        }
        public void SetReasonForValidity(byte Val)
        {
            ReasonForInValidity = Val;
        }
        private int CalculatedCheckSum = 0xFF;
        public void SetCalculatedCheckSum(int val)
        {
            CalculatedCheckSum = val;
        }
        public int GetCalculatedCheckSum()
        {
            return CalculatedCheckSum;
        }
        private byte CheckSumByte;
        public byte GetCheckSumByteInEDID()
        {
            return CheckSumByte;
        }
        public void SetCheckSumByteInEDID(byte val)
        {
            CheckSumByte = val;
        }
        double Rx, Ry, Gx, Gy, Bx, By, Wx, Wy;
        public void SetRx(double val)
        {
            Rx = val;
        }
        public double GetRx()
        {
            return Rx;
        }
        public void SetRy(double val)
        {
            Ry = val;
        }
        public double GetRy()
        {
            return Ry;
        }
        public void SetGx(double val)
        {
            Gx = val;
        }
        public double GetGx()
        {
            return Gx;
        }
        public void SetGy(double val)
        {
            Gy = val;
        }
        public double GetGy()
        {
            return Gy;
        }
        public void SetBx(double val)
        {
            Bx = val;
        }
        public double GetBx()
        {
            return Bx;
        }
        public void SetBy(double val)
        {
            By = val;
        }
        public double GetBy()
        {
            return By;
        }
        public void SetWx(double val)
        {
            Wx = val;
        }
        public double GetWx()
        {
            return Wx;
        }
        public void SetWy(double val)
        {
            Wy = val;
        }
        public double GetWy()
        {
            return Wy;
        }
        public bool IsEDIDValid()
        {
            return EDIDValid;
        }
        public void SetEDIDValidity(bool Val)
        {
            EDIDValid = Val;
        }
        private string Header;
        public string GetHeader()
        {
            return Header;
        }
        public void SetHeader(string Val)
        {
            Header = Val;
        }
        public bool IsHeaderValid()
        {
            if (Header == "INVALID" || Header == "")
                return false;
            else
                return true;
        }
        private string ManufacturerID;
        public string GetManufacturerID()
        {
            return ManufacturerID;
        }
        public void SetManufacturerID(string Val)
        {
            ManufacturerID = Val;
        }
        private String ManfacturerIDHexCode;
        public String GetManufacturerIDHexCode()
        {
            return ManfacturerIDHexCode;
        }
        public void SetManufacturerIDHexCode(String Val)
        {
            ManfacturerIDHexCode = Val;
        }
        public bool IsManufacturerIDValid()
        {
            if (ManufacturerID == "INVALID" || ManufacturerID == "")
                return false;
            else
                return true;
        }
        private byte ColorBitDepth = 0x00;
        public byte GetColorBitDepth()
        {
            return ColorBitDepth;
        }
        public void SetColorBitDepth(byte Val)
        {
            ColorBitDepth = Val;
        }
        private string strColorBitDefinitionString = "";
        public string GetstrColorBitDefinitionString()
        {
            return strColorBitDefinitionString;
        }
        public void SetstrColorBitDefinitionString(string Val)
        {
            strColorBitDefinitionString = Val;
        }
        private byte DVISupported = 0x00;
        public byte GetDVISupported()
        {
            return DVISupported;
        }
        public void SetDVISupported(byte Val)
        {
            DVISupported = Val;
        }
        private string DVISupportString = "";
        public string GetDVISupportString()
        {
            return DVISupportString;
        }
        public void SetDVISupportString(string Val)
        {
            DVISupportString = Val;
        }
        public char IsDVISupported()
        {
            if (DVISupportString == "DVI is supported")
                return 'Y';
            else
                return 'N';
        }
        public char IsHDMI_a_Supported()
        {
            if (DVISupportString == "HDMI-a is supported")
                return 'Y';
            else
                return 'N';
        }
        public char IsHDMI_b_Supported()
        {
            if (DVISupportString == "HDMI-b is supported")
                return 'Y';
            else
                return 'N';
        }
        public char IsMDDI_Supported()
        {
            if (DVISupportString == "MDDI is supported")
                return 'Y';
            else
                return 'N';
        }
        public char IsDisplayPortSupported()
        {
            if (DVISupportString == "Display Port is supported")
                return 'Y';
            else
                return 'N';
        }
        private string ProductID;
        public string GetProductID()
        {
            return ProductID;
        }
        public void SetProductID(string Val)
        {
            ProductID = Val;
        }
        public bool IsProductIDValid()
        {
            if (ProductID == "INVALID" || ProductID == "")
                return false;
            else
                return true;
        }
        private string SerialNumber;
        private bool NonZeroSerialNumber = false;
        public string GetSerialNumber()
        {
            return SerialNumber;
        }
        public void SetSerialNumber(string Val)
        {
            SerialNumber = Val;
        }
        public void SetNonZeroSerialNumber(bool Val)
        {
            NonZeroSerialNumber = Val;
        }
        public bool IsNonZeroSerialNumber()
        {
            return NonZeroSerialNumber;
        }
        private byte WeekOfManufactur = 0;
        public byte GetWeekOfManufactur()
        {
            return WeekOfManufactur;
        }
        public void SetWeekOfManufactur(byte Val)
        {
            WeekOfManufactur = Val;
        }
        private string YearOfManufacture;
        public string GetYearOfManufacture()
        {
            return YearOfManufacture;
        }
        public void SetYearOfManufacture(string Val)
        {
            YearOfManufacture = Val;
        }
        private uint VersionNumber = 0;
        public uint GetVersionNumber()
        {
            return VersionNumber;
        }
        public void SetVersionNumber(uint Val)
        {
            VersionNumber = Val;
        }
        private uint RevisionNumber = 0;
        public uint GetRevisionNumber()
        {
            return RevisionNumber;
        }
        public void SetRevisionNumber(uint Val)
        {
            RevisionNumber = Val;
        }
        private string EDIDVersion;
        public string GetEDIDVersion()
        {
            return EDIDVersion;
        }
        public void SetEDIDVersion(string Val)
        {
            EDIDVersion = Val;
        }
        private string MonitorName;
        public string GetMonitorName()
        {
            return MonitorName;
        }
        public void SetMonitorName(string Val)
        {
            MonitorName = Val;
        }
        private string ASCIIText = "";
        public string GetASCIIText()
        {
            return ASCIIText;
        }
        public void SetASCIIText(string Val)
        {
            ASCIIText = Val;
        }
        private string MonitorSerialNumber;
        public string GetMonitorSerialNumber()
        {
            return MonitorSerialNumber;
        }
        public void SetMonitorSerialNumber(string Val)
        {
            MonitorSerialNumber = Val;
        }
        private string VideoInputType;
        public string GetVideoInputType()
        {
            return VideoInputType;
        }
        public void SetVideoInputType(string Val)
        {
            VideoInputType = Val;
        }
        private string VideoInputDefintionByte;
        public string GetVideoInputDefintionByte()
        {
            return VideoInputDefintionByte;
        }
        public void SetVideoInputDefintionByte(string Val)
        {
            VideoInputDefintionByte = Val;
        }
        private string WhiteAndSyncLevel;
        public string GetWhiteAndSyncLevel()
        {
            return WhiteAndSyncLevel;
        }
        public void SetWhiteAndSyncLevel(string Val)
        {
            WhiteAndSyncLevel = Val;
        }
        private bool BlankToBlackSetup = false;
        public bool IsBlankToBlackSetupExpected()
        {
            return BlankToBlackSetup;
        }
        public void SetBlankToBlackSetup(bool val)
        {
            BlankToBlackSetup = val;
        }
        double AspectRatioPortrait = 0.0;
        public double GetAspectRatioPortrait()
        {
            return AspectRatioPortrait;
        }
        public void SetAspectRatioPortrait(double val)
        {
            AspectRatioPortrait = val;
        }
        double AspectRatioLandscape = 0.0;
        public double GetAspectRatioLandscape()
        {
            return AspectRatioLandscape;
        }
        public void SetAspectRatioLandscape(double val)
        {
            AspectRatioLandscape = val;
        }
        private bool SeparateSyncsSupported = false;
        public bool IsSeparateSyncsSupported()
        {
            return SeparateSyncsSupported;
        }
        public void SetSeparateSyncsSupported(bool val)
        {
            SeparateSyncsSupported = val;
        }
        private bool CompositeSyncSupported = false;
        public bool IsCompositeSyncSupported()
        {
            return CompositeSyncSupported;
        }
        public void SetCompositeSyncSupported(bool val)
        {
            CompositeSyncSupported = val;
        }

        private bool SyncOnGreenSupported = false;
        public bool IsSyncOnGreenSupported()
        {
            return SyncOnGreenSupported;
        }
        public void SetSyncOnGreenSupported(bool val)
        {
            SyncOnGreenSupported = val;
        }

        private bool SerrationOfVSync = false;
        public bool IsSerrationOfVSync()
        {
            return SerrationOfVSync;
        }
        public void SetSerrationOfVSync(bool val)
        {
            SerrationOfVSync = val;
        }

        private string DisplayIsProjector;
        public string GetDisplayIsProjector()
        {
            return DisplayIsProjector;
        }
        public void SetDisplayIsProjector(string Val)
        {
            DisplayIsProjector = Val;
        }

        private string MaxHorImageSize;
        public string GetMaxHorImageSize()
        {
            return MaxHorImageSize;
        }
        public void SetMaxHorImageSize(string Val)
        {
            MaxHorImageSize = Val;
        }

        private string MaxVerImageSize;
        public string GetMaxVerImageSize()
        {
            return MaxVerImageSize;
        }
        public void SetMaxVerImageSize(string Val)
        {
            MaxVerImageSize = Val;
        }

        private string DisplayGamma;
        public string GetDisplayGamma()
        {
            return DisplayGamma;
        }
        public void SetDisplayGamma(string Val)
        {
            DisplayGamma = Val;
        }

        string DMPSStandBy = "";
        public string GetDMPSStandBy()
        {
            return DMPSStandBy;
        }
        public void SetDMPSStandBy(string Val)
        {
            DMPSStandBy = Val;
        }

        string DPMSSuspend = "";
        public string GetDPMSSuspend()
        {
            return DPMSSuspend;
        }
        public void SetDPMSSuspend(string Val)
        {
            DPMSSuspend = Val;
        }

        string DPMSActiveOff = "";
        public string GetDPMSActiveOff()
        {
            return DPMSActiveOff;
        }
        public void SetDPMSActiveOff(string Val)
        {
            DPMSActiveOff = Val;
        }

        private string strDisplayColorType = "";
        public void SetstrDisplayColorType(string Val)
        {
            strDisplayColorType = Val;
        }
        public string GetstrDisplayColorType()
        {
            return strDisplayColorType;
        }

        private byte DisplayColorType = 0x00;
        public char IsDisplayMonochrome()
        {
            if ((DisplayColorType & 0x00) > 0)
                return 'Y';
            else
                return 'N';
        }
        public char IsDisplayRGBcolor()
        {
            if ((DisplayColorType & 0x08) > 0)
                return 'Y';
            else
                return 'N';
        }
        public char IsDisplayNon_RGBcolor()
        {
            if ((DisplayColorType & 0x10) > 0)
                return 'Y';
            else
                return 'N';
        }

        public void SetDisplayColorType(byte Val)
        {
            DisplayColorType = Val;
        }
        public byte GetDisplayColorType()
        {
            return DisplayColorType;
        }

        private byte ColorEncodingFormats = 0x00;
        public void SetColorEncodingFormats(byte Val)
        {
            ColorEncodingFormats = Val;
        }
        public byte GetColorEncodingFormats()
        {
            return ColorEncodingFormats;
        }
        public char IsRGB4_4_4()
        {
            if (strColorEncodingFormat == "RGB 4:4:4")
                return 'Y';
            else
                return 'N';
        }

        public char IsRGB4_4_4_YCrCb4_4_4()
        {
            if (strColorEncodingFormat == "RGB 4:4:4 and YCrCb 4:4:4")
                return 'Y';
            else
                return 'N';
        }
        public char IsRGB4_4_4_YCrCb4_2_2()
        {
            if (strColorEncodingFormat == "RGB 4:4:4 and YCrCb 4:2:2")
                return 'Y';
            else
                return 'N';
        }
        public char IsRGB4_4_4_YCrCb4_4_4_YCrCb4_2_2()
        {
            if (strColorEncodingFormat == "RGB 4:4:4 and YCrCb 4:4:4 and YCrCb 4:2:2")
                return 'Y';
            else
                return 'N';
        }
        private string strColorEncodingFormat = "";
        public void SetstrColorEncodingFormat(string Val)
        {
            strColorEncodingFormat = Val;
        }
        public string GetstrColorEncodingFormat()
        {
            return strColorEncodingFormat;
        }

        bool sRGBDefault = false;
        public void SetsRGBDefault(bool Val)
        {
            sRGBDefault = Val;
        }
        public bool GetsRGBDefault()
        {
            return sRGBDefault;
        }
        bool NPFandRRInlcuded = false;
        public void SetNPFandRRInlcuded(bool Val)
        {
            NPFandRRInlcuded = Val;
        }
        public bool GetNPFandRRInlcuded()
        {
            return NPFandRRInlcuded;
        }
        bool boolDCF = false;
        public void SetDCF(bool Val)
        {
            boolDCF = Val;
        }
        public bool GetDCF()
        {
            return boolDCF;
        }

        string Descriptor1IsPreferredTiming = "";
        public string GetDescriptor1IsPreferredTiming()
        {
            return Descriptor1IsPreferredTiming;
        }
        public void SetDescriptor1IsPreferredTiming(string Val)
        {
            Descriptor1IsPreferredTiming = Val;
        }

        string Descriptor1 = "";
        public string GetDescriptor1()
        {
            return Descriptor1;
        }
        public void SetDescriptor1(string Val)
        {
            Descriptor1 = Val;
        }

        //Monitor
        List<Byte> MonitorByteDescriptor1 = new List<Byte>();
        public List<Byte> GetMonitorByteDescriptor1()
        {
            return MonitorByteDescriptor1;
        }
        public void SetMonitorByteDescriptor1(List<Byte> Val)
        {
            MonitorByteDescriptor1 = Val;
        }
        //Monitor Range Limit
        List<Byte> MonitorRangeLimitByteDescriptor1 = new List<Byte>();
        public List<Byte> GetMonitorRangeLimitByteDescriptor1()
        {
            return MonitorRangeLimitByteDescriptor1;
        }
        public void SetMonitorRangeLimitByteDescriptor1(List<Byte> Val)
        {
            MonitorRangeLimitByteDescriptor1 = Val;
        }
        //Detailed Timings
        List<Byte> DetailedTimingByteDescriptor1 = new List<Byte>();
        public List<Byte> GetDetailedTimingByteDescriptor1()
        {
            return DetailedTimingByteDescriptor1;
        }
        public void SetDetailedTimingByteDescriptor1(List<Byte> Val)
        {
            DetailedTimingByteDescriptor1 = Val;
        }

        string ChromacityCoordinates = "";
        public string GetChromacityCoordinates()
        {
            return ChromacityCoordinates;
        }
        public void SetChromacityCoordinates(string Val)
        {
            ChromacityCoordinates = Val;
        }

        string EstablishedTimingBitmap = "";
        public string GetEstablishedTimingBitmap()
        {
            return EstablishedTimingBitmap;
        }
        public void SetEstablishedTimingBitmap(string Val)
        {
            EstablishedTimingBitmap = Val;
        }

        string StandardDisplayModes = "";
        public string GetStandardDisplayModes()
        {
            return StandardDisplayModes;
        }
        public void SetStandardDisplayModes(string Val)
        {
            StandardDisplayModes = Val;
        }

        string Descriptor1_Type = "";
        public string GetDescriptor1_Type()
        {
            return Descriptor1_Type;
        }
        public void SetDescriptor1_Type(string Val)
        {
            Descriptor1_Type = Val;
        }

        string Descriptor2_Type = "";
        public string GetDescriptor2_Type()
        {
            return Descriptor2_Type;
        }
        public void SetDescriptor2_Type(string Val)
        {
            Descriptor2_Type = Val;
        }
        string Descriptor2 = "";
        public string GetDescriptor2()
        {
            return Descriptor2;
        }
        public void SetDescriptor2(string Val)
        {
            Descriptor2 = Val;
        }
        //Monitor
        List<Byte> MonitorByteDescriptor2 = new List<Byte>();
        public List<Byte> GetMonitorByteDescriptor2()
        {
            return MonitorByteDescriptor2;
        }
        public void SetMonitorByteDescriptor2(List<Byte> Val)
        {
            MonitorByteDescriptor2 = Val;
        }
        //Monitor Range Limit
        List<Byte> MonitorRangeLimitByteDescriptor2 = new List<Byte>();
        public List<Byte> GetMonitorRangeLimitByteDescriptor2()
        {
            return MonitorRangeLimitByteDescriptor2;
        }
        public void SetMonitorRangeLimitByteDescriptor2(List<Byte> Val)
        {
            MonitorRangeLimitByteDescriptor2 = Val;
        }
        //Detailed Timings
        List<Byte> DetailedTimingByteDescriptor2 = new List<Byte>();
        public List<Byte> GetDetailedTimingByteDescriptor2()
        {
            return DetailedTimingByteDescriptor2;
        }
        public void SetDetailedTimingByteDescriptor2(List<Byte> Val)
        {
            DetailedTimingByteDescriptor2 = Val;
        }
        string Descriptor3_Type = "";
        public string GetDescriptor3_Type()
        {
            return Descriptor3_Type;
        }
        public void SetDescriptor3_Type(string Val)
        {
            Descriptor3_Type = Val;
        }
        string Descriptor3 = "";
        public string GetDescriptor3()
        {
            return Descriptor3;
        }
        public void SetDescriptor3(string Val)
        {
            Descriptor3 = Val;
        }
        //Monitor
        List<Byte> MonitorByteDescriptor3 = new List<Byte>();
        public List<Byte> GetMonitorByteDescriptor3()
        {
            return MonitorByteDescriptor3;
        }
        public void SetMonitorByteDescriptor3(List<Byte> Val)
        {
            MonitorByteDescriptor3 = Val;
        }
        //Monitor Range Limit
        List<Byte> MonitorRangeLimitByteDescriptor3 = new List<Byte>();
        public List<Byte> GetMonitorRangeLimitByteDescriptor3()
        {
            return MonitorRangeLimitByteDescriptor3;
        }
        public void SetMonitorRangeLimitByteDescriptor3(List<Byte> Val)
        {
            MonitorRangeLimitByteDescriptor3 = Val;
        }
        //Detailed Timings
        List<Byte> DetailedTimingByteDescriptor3 = new List<Byte>();
        public List<Byte> GetDetailedTimingByteDescriptor3()
        {
            return DetailedTimingByteDescriptor3;
        }
        public void SetDetailedTimingByteDescriptor3(List<Byte> Val)
        {
            DetailedTimingByteDescriptor3 = Val;
        }
        string Descriptor4_Type = "";
        public string GetDescriptor4_Type()
        {
            return Descriptor4_Type;
        }
        public void SetDescriptor4_Type(string Val)
        {
            Descriptor4_Type = Val;
        }
        string Descriptor4 = "";
        public string GetDescriptor4()
        {
            return Descriptor4;
        }
        public void SetDescriptor4(string Val)
        {
            Descriptor4 = Val;
        }
        //Monitor
        List<Byte> MonitorByteDescriptor4 = new List<Byte>();
        public List<Byte> GetMonitorByteDescriptor4()
        {
            return MonitorByteDescriptor4;
        }
        public void SetMonitorByteDescriptor4(List<Byte> Val)
        {
            MonitorByteDescriptor4 = Val;
        }
        //Monitor Range Limit
        List<Byte> MonitorRangeLimitByteDescriptor4 = new List<Byte>();
        public List<Byte> GetMonitorRangeLimitByteDescriptor4()
        {
            return MonitorRangeLimitByteDescriptor4;
        }
        public void SetMonitorRangeLimitByteDescriptor4(List<Byte> Val)
        {
            MonitorRangeLimitByteDescriptor4 = Val;
        }
        //Detailed Timings
        List<Byte> DetailedTimingByteDescriptor4 = new List<Byte>();
        public List<Byte> GetDetailedTimingByteDescriptor4()
        {
            return DetailedTimingByteDescriptor4;
        }
        public void SetDetailedTimingByteDescriptor4(List<Byte> Val)
        {
            DetailedTimingByteDescriptor4 = Val;
        }
        byte NumberOfExtensions = 0;
        public byte GetNumberOfExtensions()
        {
            return NumberOfExtensions;
        }
        public void SetNumberOfExtensions(byte Val)
        {
            NumberOfExtensions = Val;
        }
        char Timing_720_400_70 = ' ';
        public void Set_Timing_720_400_70(char info)
        {
            Timing_720_400_70 = info;
        }
        public char Get_Timing_720_400_70()
        {
            return Timing_720_400_70;
        }
        char Timing_720_400_88 = ' ';
        public void Set_Timing_720_400_88(char info)
        {
            Timing_720_400_88 = info;
        }
        public char Get_Timing_720_400_88()
        {
            return Timing_720_400_88;
        }
        char Timing_640_480_60 = ' ';
        public void Set_Timing_640_480_60(char info)
        {
            Timing_640_480_60 = info;
        }
        public char Get_Timing_640_480_60()
        {
            return Timing_640_480_60;
        }
        char Timing_640_480_67 = ' ';
        public void Set_Timing_640_480_67(char info)
        {
            Timing_640_480_67 = info;
        }
        public char Get_Timing_640_480_67()
        {
            return Timing_640_480_67;
        }
        char Timing_640_480_72 = ' ';
        public void Set_Timing_640_480_72(char info)
        {
            Timing_640_480_72 = info;
        }
        public char Get_Timing_640_480_72()
        {
            return Timing_640_480_72;
        }
        char Timing_640_480_75 = ' ';
        public void Set_Timing_640_480_75(char info)
        {
            Timing_640_480_75 = info;
        }
        public char Get_Timing_640_480_75()
        {
            return Timing_640_480_75;
        }
        char Timing_800_600_56 = ' ';
        public void Set_Timing_800_600_56(char info)
        {
            Timing_800_600_56 = info;
        }
        public char Get_Timing_800_600_56()
        {
            return Timing_800_600_56;
        }
        char Timing_800_600_60 = ' ';
        public void Set_Timing_800_600_60(char info)
        {
            Timing_800_600_60 = info;
        }
        public char Get_Timing_800_600_60()
        {
            return Timing_800_600_60;
        }
        char Timing_800_600_72Hz = ' ';
        public void Set_Timing_800_600_72Hz(char info)
        {
            Timing_800_600_72Hz = info;
        }
        public char Get_Timing_800_600_72Hz()
        {
            return Timing_800_600_72Hz;
        }
        char Timing_800_600_75Hz = ' ';
        public void Set_Timing_800_600_75Hz(char info)
        {
            Timing_800_600_75Hz = info;
        }
        public char Get_Timing_800_600_75Hz()
        {
            return Timing_800_600_75Hz;
        }
        char Timing_832_624_75Hz = ' ';
        public void Set_Timing_832_624_75Hz(char info)
        {
            Timing_832_624_75Hz = info;
        }
        public char Get_Timing_832_624_75Hz()
        {
            return Timing_832_624_75Hz;
        }
        char Timing_1024_768_87Hz = ' ';
        public void Set_Timing_1024_768_87Hz(char info)
        {
            Timing_1024_768_87Hz = info;
        }
        public char Get_Timing_1024_768_87Hz()
        {
            return Timing_1024_768_87Hz;
        }
        char Timing_1024_768_60Hz = ' ';
        public void Set_Timing_1024_768_60Hz(char info)
        {
            Timing_1024_768_60Hz = info;
        }
        public char Get_Timing_1024_768_60Hz()
        {
            return Timing_1024_768_60Hz;
        }
        char Timing_1024_768_72Hz = ' ';
        public void Set_Timing_1024_768_72Hz(char info)
        {
            Timing_1024_768_72Hz = info;
        }
        public char Get_Timing_1024_768_72Hz()
        {
            return Timing_1024_768_72Hz;
        }
        char Timing_1024_768_75Hz = ' ';
        public void Set_Timing_1024_768_75Hz(char info)
        {
            Timing_1024_768_75Hz = info;
        }
        public char Get_Timing_1024_768_75Hz()
        {
            return Timing_1024_768_75Hz;
        }
        char Timing_1152_870_75Hz = ' ';
        public void Set_Timing_1152_870_75Hz(char info)
        {
            Timing_1152_870_75Hz = info;
        }
        public char Get_Timing_1152_870_75Hz()
        {
            return Timing_1152_870_75Hz;
        }
        char Timing_1280_1024_75Hz = ' ';
        public void Set_Timing_1280_1024_75Hz(char info)
        {
            Timing_1280_1024_75Hz = info;
        }
        public char Get_Timing_1280_1024_75Hz()
        {
            return Timing_1280_1024_75Hz;
        }
        int[] HorizontalActivePixels = new int[8];
        public void SetHorizontalActivePixels(int index, int value)
        {
            HorizontalActivePixels[index] = value;
        }
        public int GetHorizontalActivePixels(int index)
        {
            return HorizontalActivePixels[index];
        }
        string[] ImageAspectRatio = new string[8];
        public void SetImageAspectRatio(int index, string value)
        {
            ImageAspectRatio[index] = value;
        }
        public string GetImageAspectRatio(int index)
        {
            return ImageAspectRatio[index];
        }
        int[] RefreshRate = new int[8];
        public void SetRefreshRate(int index, int value)
        {
            RefreshRate[index] = value;
        }
        public int GetRefreshRate(int index)
        {
            return RefreshRate[index];
        }
        UInt16 pixelClock = 0;
        public void SetpixelClock(UInt16 Val)
        {
            pixelClock = Val;
        }
        public UInt16 GetpixelClock()
        {
            return pixelClock;
        }
        UInt16 HorizontalActivePixelsOfDescriptor = 0;
        public void SetHorizontalActivePixelsOfDescriptor(UInt16 Val)
        {
            HorizontalActivePixelsOfDescriptor = Val;
        }
        public UInt16 GetHorizontalActivePixelsOfDescriptor()
        {
            return HorizontalActivePixelsOfDescriptor;
        }
        UInt16 HorizontalBlankingPixels = 0;
        public void SetHorizontalBlankingPixels(UInt16 Val)
        {
            HorizontalBlankingPixels = Val;
        }
        public UInt16 GetHorizontalBlankingPixels()
        {
            return HorizontalBlankingPixels;
        }
        UInt16 VerticalActivePixels = 0;
        public void SetVerticalActivePixels(UInt16 Val)
        {
            VerticalActivePixels = Val;
        }
        public UInt16 GetVerticalActivePixels()
        {
            return VerticalActivePixels;
        }
        UInt16 VerticalBlankingPixels = 0;
        public void SetVerticalBlankingPixels(UInt16 Val)
        {
            VerticalBlankingPixels = Val;
        }
        public UInt16 GetVerticalBlankingPixels()
        {
            return VerticalBlankingPixels;
        }
        UInt16 HorizontalSyncOffset = 0;
        public void SetHorizontalSyncOffset(UInt16 Val)
        {
            HorizontalSyncOffset = Val;
        }
        public UInt16 GetHorizontalSyncOffset()
        {
            return HorizontalSyncOffset;
        }
        UInt16 HorizontalSyncPulseWidth = 0;
        public void SetHorizontalSyncPulseWidth(UInt16 Val)
        {
            HorizontalSyncPulseWidth = Val;
        }
        public UInt16 GetHorizontalSyncPulseWidth()
        {
            return HorizontalSyncPulseWidth;
        }
        UInt16 VerticalSyncOffset = 0;
        public void SetVerticalSyncOffset(UInt16 Val)
        {
            VerticalSyncOffset = Val;
        }
        public UInt16 GetVerticalSyncOffset()
        {
            return VerticalSyncOffset;
        }
        UInt16 VerticalSyncPulseWidth = 0;
        public void SetVerticalSyncPulseWidth(UInt16 Val)
        {
            VerticalSyncPulseWidth = Val;
        }
        public UInt16 GetVerticalSyncPulseWidth()
        {
            return VerticalSyncPulseWidth;
        }
        UInt16 HorizontalDisplaySize = 0;
        public void SetHorizontalDisplaySize(UInt16 Val)
        {
            HorizontalDisplaySize = Val;
        }
        public UInt16 GetHorizontalDisplaySize()
        {
            return HorizontalDisplaySize;
        }
        UInt16 VerticalDisplaySize = 0;
        public void SetVerticalDisplaySize(UInt16 Val)
        {
            VerticalDisplaySize = Val;
        }
        public UInt16 GetVerticalDisplaySize()
        {
            return VerticalDisplaySize;
        }
        byte HorizontalBorderPixels = 0;
        public void SetHorizontalBorderPixels(byte Val)
        {
            HorizontalBorderPixels = Val;
        }
        public byte GetHorizontalBorderPixels()
        {
            return HorizontalBorderPixels;
        }
        byte VerticalBorderPixels = 0;
        public void SetVerticalBorderPixels(byte Val)
        {
            VerticalBorderPixels = Val;
        }
        public byte GetVerticalBorderPixels()
        {
            return VerticalBorderPixels;
        }
        bool Interlaced = false;
        public void SetInterlaced(bool Val)
        {
            Interlaced = Val;
        }
        public bool GetInterlaced()
        {
            return Interlaced;
        }
        byte SteroModeBits = 0;
        public void SetSteroModeBits(byte Val)
        {
            SteroModeBits = Val;
        }
        public byte GetSteroModeBits()
        {
            return SteroModeBits;
        }
        public bool IsNormalDisplay_NoStereo()
        {
            if (SteroModeBits == 0 || SteroModeBits == 1)
            {
                return true;
            }
            else
                return false;
        }
        public bool IsFieldsequentialstereo_rightimage()
        {
            if (SteroModeBits == 2)
            {
                return true;
            }
            else
                return false;
        }
        public bool IsFieldsequentialstereo_leftimage()
        {
            if (SteroModeBits == 4)
            {
                return true;
            }
            else
                return false;
        }
        public bool IsTwowayinterleavedstereo_rightimage()
        {
            if (SteroModeBits == 3)
            {
                return true;
            }
            else
                return false;
        }
        public bool IsTwowayinterleavedstereo_leftimage()
        {
            if (SteroModeBits == 5)
            {
                return true;
            }
            else
                return false;
        }
        public bool IsFourwayinterleavedstereo()
        {
            if (SteroModeBits == 6)
            {
                return true;
            }
            else
                return false;
        }
        public bool IsSidebySideinterleavedstereo()
        {
            if (SteroModeBits == 7)
            {
                return true;
            }
            else
                return false;
        }
        byte AnalogDigitalSignalDefinitionBits = 0;
        public void SetAnalogDigitalSignalDefinitionBits(byte Val)
        {
            AnalogDigitalSignalDefinitionBits = Val;
        }
        public byte GetAnalogDigitalSignalDefinitionBits()
        {
            return AnalogDigitalSignalDefinitionBits;
        }
        public bool IsAnalogCompositeSync()
        {
            if ((AnalogDigitalSignalDefinitionBits & 0x08) == 0)
            {
                if ((AnalogDigitalSignalDefinitionBits & 0x04) > 0)
                    return true;
                else
                    return false;
            }
            return false;
        }
        public bool IsBipolarAnalogCompositeSync()
        {
            if ((AnalogDigitalSignalDefinitionBits & 0x08) == 0)
            {
                if ((AnalogDigitalSignalDefinitionBits & 0x04) == 0)
                    return true;
                else
                    return false;
            }
            return false;
        }
        public bool IsAnalogWithoutSerrations()
        {
            if ((AnalogDigitalSignalDefinitionBits & 0x08) == 0)
            {
                if ((AnalogDigitalSignalDefinitionBits & 0x02) == 0)
                    return true;
                else
                    return false;
            }
            return false;
        }
        public bool IsAnalogWithSerrations()
        {
            if ((AnalogDigitalSignalDefinitionBits & 0x08) == 0)
            {
                if ((AnalogDigitalSignalDefinitionBits & 0x02) > 0)
                    return true;
                else
                    return false;
            }
            return false;
        }
        public bool IsSyncOnGreenSignalonly()
        {
            if ((AnalogDigitalSignalDefinitionBits & 0x08) == 0)
            {
                if ((AnalogDigitalSignalDefinitionBits & 0x01) == 0)
                    return true;
                else
                    return false;
            }
            return false;
        }
        public bool IsSyncOnallthreeRGBvideosignals()
        {
            if ((AnalogDigitalSignalDefinitionBits & 0x08) == 0)
            {
                if ((AnalogDigitalSignalDefinitionBits & 0x01) > 0)
                    return true;
                else
                    return false;
            }
            return false;
        }
        public bool IsDigitalCompositeSync()
        {
            if ((AnalogDigitalSignalDefinitionBits & 0x08) > 0)
            {
                if ((AnalogDigitalSignalDefinitionBits & 0x04) == 0)
                    return true;
                else
                    return false;
            }
            return false;
        }
        public bool IsDigitalWithoutSerrations()
        {
            if ((AnalogDigitalSignalDefinitionBits & 0x08) > 0)
            {
                if ((AnalogDigitalSignalDefinitionBits & 0x04) == 0)
                {
                    if ((AnalogDigitalSignalDefinitionBits & 0x02) == 0)
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }
            return false;
        }
        public bool IsDigitalWithSerrations()
        {
            if ((AnalogDigitalSignalDefinitionBits & 0x08) > 0)
            {
                if ((AnalogDigitalSignalDefinitionBits & 0x04) == 0)
                {
                    if ((AnalogDigitalSignalDefinitionBits & 0x02) > 0)
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }
            return false;
        }
        public bool IsDigitalSeparateSync()
        {
            if ((AnalogDigitalSignalDefinitionBits & 0x08) > 0)
            {
                if ((AnalogDigitalSignalDefinitionBits & 0x04) > 0)
                    return true;
                else
                    return false;
            }
            return false;
        }
        public bool IsDigitalVerticalSyncPositive()
        {
            if ((AnalogDigitalSignalDefinitionBits & 0x08) > 0)
            {
                if ((AnalogDigitalSignalDefinitionBits & 0x04) > 0)
                {
                    if ((AnalogDigitalSignalDefinitionBits & 0x02) > 0)
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }
            return false;
        }
        public bool IsDigitalHorizontalSyncPositive()
        {
            if ((AnalogDigitalSignalDefinitionBits & 0x08) > 0)
            {
                if ((AnalogDigitalSignalDefinitionBits & 0x01) > 0)
                    return true;
                else
                    return false;
            }
            return false;
        }
        public string ExtractEDIDContentsInCSVFormat()
        {
            string EDIDCSVFormat = "";
            EDIDCSVFormat += Header;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += ManufacturerID;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += ProductID;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += SerialNumber;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += WeekOfManufactur.ToString();
            EDIDCSVFormat += ",";

            EDIDCSVFormat += YearOfManufacture;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += EDIDVersion;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += MonitorName;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += VideoInputDefintionByte;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += VideoInputType;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += WhiteAndSyncLevel;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += DisplayIsProjector;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += MaxHorImageSize;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += MaxVerImageSize;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += DisplayGamma;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += DMPSStandBy;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += DPMSSuspend;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += DPMSActiveOff;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += "Hex(" + ChromacityCoordinates + ")";
            EDIDCSVFormat += ",";

            EDIDCSVFormat += "Hex(" + EstablishedTimingBitmap + ")";
            EDIDCSVFormat += ",";

            EDIDCSVFormat += "Hex(" + StandardDisplayModes + ")";
            EDIDCSVFormat += ",";

            EDIDCSVFormat += Descriptor1IsPreferredTiming;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += Descriptor1_Type;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += Descriptor1;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += Descriptor2_Type;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += Descriptor2;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += Descriptor3_Type;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += Descriptor3;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += Descriptor4_Type;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += Descriptor4;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += NumberOfExtensions.ToString();
            EDIDCSVFormat += ",";

            EDIDCSVFormat += CheckSumByte.ToString();
            EDIDCSVFormat += ",";

            EDIDCSVFormat += CalculatedCheckSum.ToString();
            EDIDCSVFormat += ",";

            /*EDIDCSVFormat += (CalculatedCheckSum % 256).ToString();
            EDIDCSVFormat += ",";*/

            EDIDCSVFormat += Rx.ToString();
            EDIDCSVFormat += ",";

            EDIDCSVFormat += Ry.ToString();
            EDIDCSVFormat += ",";

            EDIDCSVFormat += Gx.ToString();
            EDIDCSVFormat += ",";

            EDIDCSVFormat += Gy.ToString();
            EDIDCSVFormat += ",";

            EDIDCSVFormat += Bx.ToString();
            EDIDCSVFormat += ",";

            EDIDCSVFormat += By.ToString();
            EDIDCSVFormat += ",";

            EDIDCSVFormat += Wx.ToString();
            EDIDCSVFormat += ",";

            EDIDCSVFormat += Wy.ToString();
            EDIDCSVFormat += ",";

            EDIDCSVFormat += MonitorSerialNumber;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += ASCIIText;
            EDIDCSVFormat += ",";

            return EDIDCSVFormat;
        }
        String m_PhysicalAddress = String.Empty;
        public String GetPhysicalAddress()
        {
            return m_PhysicalAddress;
        }
        public void SetPhysicalAddress(String Val)
        {
            m_PhysicalAddress = Val;
        }
        //CEC Physical Address
        List<Byte> PhysicalAddress = new List<Byte>();
        public List<Byte> GetPhysicalAddressByteData()
        {
            return PhysicalAddress;
        }
        public void SetPhysicalAddressByteData(List<Byte> Val)
        {
            PhysicalAddress = Val;
        }
        private Boolean m_ExistExtendedDataBlock = false;
        public Boolean GetExtendedDataBlock()
        {
            return m_ExistExtendedDataBlock;
        }
        public void SetExtendedDataBlock(Boolean Val)
        {
            m_ExistExtendedDataBlock = Val;
        }
    }
}
