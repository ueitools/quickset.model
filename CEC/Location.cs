﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Quickset.Model.CEC
{    
    public class Location
    {
        private String m_Region = String.Empty;
        public String Region
        {
            get { return m_Region; }
            set { m_Region = value; }
        }
        private List<String> m_Country;
        public List<String> Country
        {
            get { return m_Country; }
            set { m_Country = value; }
        }
    }
    public class Locations : List<Location>
    {
    }
}
