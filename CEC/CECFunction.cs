﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Quickset.Model.CEC
{
    public class CECFunction
    {
        private Int32 m_SlNo = 0;
        public Int32 SlNo
        {
            get { return m_SlNo; }
            set { m_SlNo = value; }
        }
        private String m_Key = String.Empty;
        public String Key
        {
            get { return m_Key; }
            set { m_Key = value; }
        }
        private String m_Command = String.Empty;
        public String Command
        {
            get { return m_Command; }
            set { m_Command = value; }
        }
        private String m_CommandWorked = String.Empty;
        public String CommandWorked
        {
            get { return m_CommandWorked; }
            set { m_CommandWorked = value; }
        }
        private String m_Status = String.Empty;
        public String Status
        {
            get { return m_Status; }
            set { m_Status = value; }
        }
        private String m_AcknowladgementReceived = String.Empty;
        public String AcknowladgementReceived
        {
            get { return m_AcknowladgementReceived; }
            set { m_AcknowladgementReceived = value; }
        }
        private Char m_IsToggle = 'N';
        public Char IsToggel
        {
            get { return m_IsToggle; }
            set { m_IsToggle = value; }
        }
        private String m_TestedDateTime = String.Empty;
        public String TestedDateTime
        {
            get { return m_TestedDateTime; }
            set { m_TestedDateTime = value; }
        }
        private Int32 m_RuleMaskWeightage = 0;
        public Int32 RuleMaskWeightage
        {
            get { return m_RuleMaskWeightage; }
            set { m_RuleMaskWeightage = value; }
        }
    }
    public class CECFunctions : List<CECFunction>
    {
    }
}