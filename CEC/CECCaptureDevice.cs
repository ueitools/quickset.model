﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Quickset.Model.CEC
{
    public class CECCaptureDevice
    {
        private int m_SlNo = 0;
        public int SlNo
        {
            get { return m_SlNo; }
            set { m_SlNo = value; }
        }
        private String m_MainDevice = String.Empty;
        public String MainDevice
        {
            get { return m_MainDevice; }
            set { m_MainDevice = value; }
        }
        private String m_SubDevice = String.Empty;
        public String SubDevice
        {
            get { return m_SubDevice; }
            set { m_SubDevice = value; }
        }
        private String m_Brand = String.Empty;
        public String Brand
        {
            get { return m_Brand; }
            set { m_Brand = value; }
        }
        private String m_Model = String.Empty;
        public String Model
        {
            get { return m_Model; }
            set { m_Model = value; }
        }
        private String m_Region = String.Empty;
        public String Region
        {
            get { return m_Region; }
            set { m_Region = value; }
        }
        private String m_Country = String.Empty;
        public String Country
        {
            get { return m_Country; }
            set { m_Country = value; }
        }
        private Char m_IsCECPresent = 'N';
        public Char CECPresent
        {
            get { return m_IsCECPresent; }
            set { m_IsCECPresent = value; }
        }
        private Char m_CECEnabled = 'N';
        public Char CECEnabled
        {
            get { return m_CECEnabled; }
            set { m_CECEnabled = value; }
        }
        private String m_VendorID = String.Empty;
        public String VendorID
        {
            get { return m_VendorID; }
            set { m_VendorID = value; }
        }
        private String m_OSDName = String.Empty;
        public String OSDName
        {
            get { return m_OSDName; }
            set { m_OSDName = value; }
        }
        private String m_EDID = String.Empty;
        public String EDID
        {
            get { return m_EDID; }
            set { m_EDID = value; }
        }
        private String m_CECVersion = String.Empty;
        public String CECVersion
        {
            get { return m_CECVersion; }
            set { m_CECVersion = value; }
        }
        private String m_PodID = String.Empty;
        public String PodID
        {
            get { return m_PodID; }
            set { m_PodID = value; }
        }
        private String m_ToolVersion = String.Empty;
        public String ToolVersion
        {
            get { return m_ToolVersion; }
            set { m_ToolVersion = value; }
        }
        private String m_LogicalAddress = String.Empty;
        public String LogicalAddress
        {
            get { return m_LogicalAddress; }
            set { m_LogicalAddress = value; }
        }
        private String m_DeviceConfiguration = String.Empty;
        public String DeviceConfiguration
        {
            get { return m_DeviceConfiguration; }
            set { m_DeviceConfiguration = value; }
        }
        private String m_Comment = String.Empty;
        public String Comment
        {
            get { return m_Comment; }
            set { m_Comment = value; }
        }
        private Int32  m_RuleNo = 0;
        public Int32 RuleNo
        {
            get { return m_RuleNo; }
            set { m_RuleNo = value; }
        }
        private String m_ID = String.Empty;
        public String ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        private String m_DiscreteID = String.Empty;
        public String DicreteID
        {
            get { return m_DiscreteID; }
            set { m_DiscreteID = value; }
        }
        private Int32 m_RuleMask = 0;
        public Int32 RuleMask
        {
            get { return m_RuleMask; }
            set { m_RuleMask = value; }
        }
    }
    public class CECCaptureDevices : List<CECCaptureDevice>
    {
    }
}