﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Quickset.Model.CEC
{
    public class DeviceType
    {
        private String m_MainDevice = String.Empty;
        public String MainDevice
        {
            get { return m_MainDevice; }
            set { m_MainDevice = value; }
        }
        private List<String> m_SubDevice;
        public List<String> SubDevice
        {
            get { return m_SubDevice; }
            set { m_SubDevice = value; }
        }
    }
    public class DeviceTypes : List<DeviceType>
    {
    }
}
