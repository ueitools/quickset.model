﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Quickset.Model.CEC
{
    public class CECKey
    {
        private Int32 m_SlNo = 0;
        public Int32 SlNo
        {
            get { return m_SlNo; }
            set { m_SlNo = value; }
        }
        private String m_Key = String.Empty;
        public String Key
        {
            get { return m_Key; }
            set { m_Key = value; }
        }
        private String m_Command = String.Empty;
        public String Command
        {
            get { return m_Command; }
            set { m_Command = value; }
        }
        private String m_KeyGroup;
        public String KeyGroup
        {
            get { return m_KeyGroup; }
            set { m_KeyGroup = value; }
        }
        private CECRuleMaskTypes m_RuleMaskType = CECRuleMaskTypes.DEFAULT;
        public CECRuleMaskTypes RuleMaskType
        {
            get { return m_RuleMaskType; }
            set { m_RuleMaskType = value; }
        }
    }
    public class CECKeys : List<CECKey>
    {
    }
}