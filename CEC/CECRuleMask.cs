﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Quickset.Model.CEC
{
    public enum CECRuleMaskTypes
    {
        DEFAULT = 0,                                            //0
        MUTE = 1,                                               //2
        VOLUME = MUTE << 1,                                     //VOLUME UP and VOLUME DOWN PRESENT OR ELSE 0
        ACTIVE_SOURCE = VOLUME << 1,                            // 4 
        POWER_OFF = ACTIVE_SOURCE << 1,                         //
        STANDBY = POWER_OFF << 1,                               //
        TVO = STANDBY << 1,                                     //
        IVO = TVO << 1,                                         //
        POWER = IVO << 1,                                       //
        POWER_ON = POWER << 1,                                  //
        SYSTEM_AUDIO_MODE = POWER_ON << 1,                      //
        SET_STREAM_PATH = SYSTEM_AUDIO_MODE << 1,               //
        SYSTEM_AUDIO_MODE_REQUEST = SET_STREAM_PATH << 1,       //
        GIVE_POWER_STATUS = SYSTEM_AUDIO_MODE_REQUEST << 1      //
    }
}