﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Quickset.Model.InfoFrame
{
    public class InfoframeEntity
    {
        private int InfoFrameRID = 0;

        public int EntryDataSize { get; set; }
        public string UEIDeviceType { get; set; }
        public string InfoFrameRawData { get; set; }
        public int InfoFrameType { get; set; }
        public int InfoFrameFieldCount { get; set; }
        public string SIFName { get; set; }
        public string SIFDescription { get; set; }
        public string VendorID { get; set; }

        public int ProviderCount
        {

            get
            {
                return GetProviderCount();
            }

        }

        public Dictionary<string,List<string>> ProvidersWithCB { get; set; }

        public int ControlBlockCount
        {
            get
            {
                return ControlBlocks.Count;

            }

        }

        public List<string> ControlBlocks { get; set; }

        public InfoframeEntity()
        {
            this.ProvidersWithCB = new Dictionary<string, List<string>>();
            this.ControlBlocks = new List<string>();
        }

        private int GetProviderCount()
        {
           return  ProvidersWithCB.SelectMany(x => x.Value).Distinct().Count();
        }

        public void SetInfoFrameIDForUpdate(int infoFrameRecordId)
        {
            if (infoFrameRecordId!=0)
            {
                this.InfoFrameRID = infoFrameRecordId;
            }
        }

        public int GetInfoFrameRecordID()
        {
            return this.InfoFrameRID;
        }

        public bool Validate(out string message)
        {
            message = String.Empty;

            if (String.IsNullOrEmpty(UEIDeviceType))
            {
                message = "UEI Device type is empty.";
                return false;
            }
            if (this.InfoFrameType == 0)
            {
                message = "InfoFrame type is missing";
                return false;
            }
            if (this.InfoFrameFieldCount == 0)
            {
                message = "InfoFrame field count is missing.";
                return false;
            }
            else
            {
                if (this.InfoFrameType == 19)
                {
                    if (this.InfoFrameFieldCount != 2)
                    {
                        message = "InfoFrame field count value should be 2 while InfoFrame type is 19.";
                        return false;
                    }
                }
            }


            if (String.IsNullOrEmpty(SIFName))
            {
                message = "SIFName is missing.";
                return false;
            }

            if (String.IsNullOrEmpty(SIFDescription))
            {
                message = "SIFDescription is missing.";

                return false;
            }

            if (String.IsNullOrEmpty(VendorID))
            {
                message = "Manufacturer name is missing.";
                return false;
            }

            if (ControlBlocks.Count == 0)
            {
                message = "Control blocks are missing.";
                return false;
            }

            if (ProvidersWithCB.Count == 0)
            {
                message = "Providers are missing.";
                return false;
            }
            else
            {
                //check if providers are attached to all the control blocks
                if (!ControlBlocks.Count.Equals(ProvidersWithCB.Count))
                {
                    message = "Providers are not attached to all control blocks.";
                    return false;
                }

            }

            //default
            return true;
        }

        public InfoframeDBInput GenerateDBInput()
        {
            InfoframeDBInput dbInput = new InfoframeDBInput
            {
                EntryDataSize = this.EntryDataSize,
                UEIDeviceType = this.UEIDeviceType[0],
                InfoFrameRawData = this.InfoFrameRawData,
                InfoFrameType = this.InfoFrameType,
                InfoFrameFieldCount = this.InfoFrameFieldCount,
                SIFName = this.SIFName,
                SIFDescription = this.SIFDescription,
                VendorID = this.VendorID,
                ProviderCount = this.ProviderCount,
                ControlBlockCount = this.ControlBlockCount
            };
            //add provider
            StringBuilder providerBuilder = new StringBuilder();
            foreach (string controlBlock in this.ProvidersWithCB.Keys)
            {
                List<string> providerList = this.ProvidersWithCB[controlBlock];
                int controlBlockNumber = this.ControlBlocks.IndexOf(controlBlock) + 1;
                
                foreach (string provider in providerList)
                {
                    providerBuilder.Append(provider + ":" + controlBlockNumber.ToString("D2") + ";");
                }
               
            }
            //remove ; from end
            if (providerBuilder.Length > 0)
                providerBuilder.Remove(providerBuilder.Length - 1, 1);

            dbInput.Providers = providerBuilder.ToString();

            //control blocks
            dbInput.ControlBlock = String.Join("", this.ControlBlocks);

            return dbInput;
        }

        public static InfoframeEntity GenerateEntity(InfoframeDBInput input)
        {
            InfoframeEntity entity = new InfoframeEntity
            {
                EntryDataSize = input.EntryDataSize,
                UEIDeviceType = Convert.ToString(input.UEIDeviceType),
                InfoFrameRawData = input.InfoFrameRawData,
                InfoFrameType = input.InfoFrameType,
                InfoFrameFieldCount = input.InfoFrameFieldCount,
                SIFDescription = input.SIFDescription,
                SIFName = input.SIFName,
                VendorID = input.VendorID
            };

            //add control block
            string controlBlockStr =input.ControlBlock;
            //remove the '0' padded with single digit rule numbers
            string[] controlBlockTemp = controlBlockStr.Split(new char[] { '#' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string item in controlBlockTemp)
            {
               string[] splitCB = item.Split(':');
               int correctedRuleNum = Convert.ToInt32(splitCB[2]);
               string correctedCB = string.Format("{0}:{1}:{2}#", splitCB[0], splitCB[1], correctedRuleNum);
               entity.ControlBlocks.Add(correctedCB);
            }


           //add providers
            string providerStr = input.Providers;
            var providerWithCBList = providerStr.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries).Select(x => new { provider = x.Split(':') });
            foreach (var item in providerWithCBList)
            {
                string provider = item.provider[0];
                int cbNum = Int32.Parse(item.provider[1]);

                string controlBlock = entity.ControlBlocks[cbNum - 1];
                if (entity.ProvidersWithCB.ContainsKey(controlBlock))
                {
                    entity.ProvidersWithCB[controlBlock].Add(provider);
                }
                else
                {
                    entity.ProvidersWithCB.Add(controlBlock, new List<string>() { provider });
                }
            }


            return entity;
        }
    }
}
