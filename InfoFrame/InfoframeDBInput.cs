﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Quickset.Model.InfoFrame
{
    public class InfoframeDBInput
    {
        private const string constInfoFrameRID = "Infoframe_RID";
        private const string constUEIInfoFrameType = "UEIInfo Frame Type";
        private const string constInfoFrameFieldCount = "Info frame Fields Count";
        private const string constSIFName = "SIF Name";
        private const string constSIFDesc = "SIF Description";
        private const string constManufacturer = "Manufacturer & Vender ID(s)";
        private const string constProviders = "Providers";
        private const string constControlBlock = "Region+Control block";

        public int InfoFrameRecordDbID { get; set; }
        public int EntryDataSize { get; set; }
        public char UEIDeviceType { get; set; }
        public string InfoFrameRawData { get; set; }
        public int InfoFrameType { get; set; }
        public int InfoFrameFieldCount { get; set; }
        public string SIFName { get; set; }
        public string SIFDescription { get; set; }
        public string VendorID { get; set; }
        public string ControlBlock { get; set; }
        public string Providers { get; set; }
        public int ProviderCount { get; set; }
        public int ControlBlockCount { get; set; }

        public DataTable GetInputInTableFormat()
        {
            DataTable dtInput = new DataTable();
            dtInput.Columns.Add(constUEIInfoFrameType);
            dtInput.Columns.Add(constInfoFrameFieldCount);
            dtInput.Columns.Add(constSIFName);
            dtInput.Columns.Add(constSIFDesc);
            dtInput.Columns.Add(constManufacturer);
            dtInput.Columns.Add(constProviders);
            dtInput.Columns.Add(constControlBlock);

            DataRow row = dtInput.NewRow();
            row[constUEIInfoFrameType] = this.InfoFrameType;
            row[constInfoFrameFieldCount] = this.InfoFrameFieldCount;
            row[constSIFName] = this.SIFName;
            row[constSIFDesc] = this.SIFDescription;
            row[constManufacturer] = this.VendorID;
            row[constProviders] = this.Providers;
            row[constControlBlock] = this.ControlBlock;

            dtInput.Rows.Add(row);

            return dtInput;
        }

        public DataTable GetUpdateInputInTableFormat()
        {
            DataTable dtInput = new DataTable();
            dtInput.Columns.Add(constInfoFrameRID);
            dtInput.Columns.Add(constUEIInfoFrameType);
            dtInput.Columns.Add(constInfoFrameFieldCount);
            dtInput.Columns.Add(constSIFName);
            dtInput.Columns.Add(constSIFDesc);
            dtInput.Columns.Add(constManufacturer);
            dtInput.Columns.Add(constProviders);
            dtInput.Columns.Add(constControlBlock);

            DataRow row = dtInput.NewRow();
            row[constInfoFrameRID] = this.InfoFrameRecordDbID;
            row[constUEIInfoFrameType] = this.InfoFrameType;
            row[constInfoFrameFieldCount] = this.InfoFrameFieldCount;
            row[constSIFName] = this.SIFName;
            row[constSIFDesc] = this.SIFDescription;
            row[constManufacturer] = this.VendorID;
            row[constProviders] = this.Providers;
            row[constControlBlock] = this.ControlBlock;

            dtInput.Rows.Add(row);

            return dtInput;
        }
    }
}
