﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Quickset.Model.IP
{
    public class IPInfo
    {
        #region Properties
        private Int32 m_SlNo = 0;
        public Int32 SlNo
        {
            get { return m_SlNo; }
            set { m_SlNo = value; }
        }
        private Int32 m_DataEntrySize = 0;
        public Int32 DataEntrySize
        {
            get { return m_DataEntrySize; }
            set { m_DataEntrySize = value; }
        }
        private String m_Manufacturer = String.Empty;
        public String Manufacturer
        {
            get { return m_Manufacturer; }
            set { m_Manufacturer = value; }
        }
        private String m_URL = String.Empty;
        public String URL
        {
            get { return m_URL; }
            set { m_URL = value; }
        }
        private String m_FirendlyName = String.Empty;
        public String FriendlyName
        {
            get { return m_FirendlyName; }
            set { m_FirendlyName = value; }
        }
        private String m_ModelName = String.Empty;
        public String ModelName
        {
            get { return m_ModelName; }
            set { m_ModelName = value; }
        }
        private String m_ModelNumber = String.Empty;
        public String ModelNumber
        {
            get { return m_ModelNumber; }
            set { m_ModelNumber = value; }
        }
        private String m_ModelDescription = String.Empty;
        public String ModelDescription
        {
            get { return m_ModelDescription; }
            set { m_ModelDescription = value; }
        }
        private String m_BrandIdField = String.Empty;
        public String BrandIdField
        {
            get { return m_BrandIdField; }
            set { m_BrandIdField = value; }
        }
        private String m_ModelIdField = String.Empty;
        public String ModelIdField
        {
            get { return m_ModelIdField; }
            set { m_ModelIdField = value; }
        }
        private String m_UEIDeviceTypeId = String.Empty;
        public String UEIDeviceTypeId
        {
            get { return m_UEIDeviceTypeId; }
            set { m_UEIDeviceTypeId = value; }
        }
        private Int32 m_Count = 0;
        public Int32 Count
        {
            get { return m_Count; }
            set { m_Count = value; }
        }
        private String m_CountrolBlock = String.Empty;
        public String CountrolBlock
        {
            get { return m_CountrolBlock; }
            set { m_CountrolBlock = value; }
        }
        private String m_Country = String.Empty;
        public String Country
        {
            get { return m_Country; }
            set { m_Country = value; }
        }
        private String m_Id = String.Empty;
        public String ID
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        private String m_Rules = String.Empty;
        public String Rules
        {
            get { return m_Rules; }
            set { m_Rules = value; }
        }
        private String m_CustomFPType0x00 = String.Empty;
        public String CustomFPType0x00 { get { return m_CustomFPType0x00; } set { m_CustomFPType0x00 = value; } }
        private String m_CustomFPType0x01 = String.Empty;
        public String CustomFPType0x01 { get { return m_CustomFPType0x01; } set { m_CustomFPType0x01 = value; } }
        private String m_CustomFPType0x02 = String.Empty;
        public String CustomFPType0x02 { get { return m_CustomFPType0x02; } set { m_CustomFPType0x02 = value; } }
        private String m_CustomFPType0x03 = String.Empty;
        public String CustomFPType0x03 { get { return m_CustomFPType0x03; } set { m_CustomFPType0x03 = value; } }
        private String m_CustomFP   = String.Empty;
        public String CustomFP          { get { return m_CustomFP; } set { m_CustomFP = value; } }
        #endregion
    }
}